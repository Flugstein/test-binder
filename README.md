# Lattice ZK

Click the badge to open the notebook in Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fflugstein%2Ftest-binder.git/HEAD?filepath=notebooks%2Flattice-zk.ipynb)


This uses the following gitlab deploy token, granting read access to the repo:

```
https://gitlab+deploy-token-3604391:KCzRtJqDwxYMQRzDJegT@gitlab.com/flugstein/test-binder.git
```
